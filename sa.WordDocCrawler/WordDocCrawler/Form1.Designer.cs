﻿namespace WordDocCrawler
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnScan = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.cbNSA = new System.Windows.Forms.CheckBox();
            this.cbEUR = new System.Windows.Forms.CheckBox();
            this.cbPAC = new System.Windows.Forms.CheckBox();
            this.cbLUB = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Files Check:";
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(142, 90);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(75, 23);
            this.btnScan.TabIndex = 2;
            this.btnScan.Text = "Scan";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(82, 46);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(253, 20);
            this.txtFile.TabIndex = 3;
            // 
            // cbNSA
            // 
            this.cbNSA.AutoSize = true;
            this.cbNSA.Location = new System.Drawing.Point(56, 12);
            this.cbNSA.Name = "cbNSA";
            this.cbNSA.Size = new System.Drawing.Size(48, 17);
            this.cbNSA.TabIndex = 4;
            this.cbNSA.Text = "NSA";
            this.cbNSA.UseVisualStyleBackColor = true;
            // 
            // cbEUR
            // 
            this.cbEUR.AutoSize = true;
            this.cbEUR.Location = new System.Drawing.Point(120, 12);
            this.cbEUR.Name = "cbEUR";
            this.cbEUR.Size = new System.Drawing.Size(49, 17);
            this.cbEUR.TabIndex = 5;
            this.cbEUR.Text = "EUR";
            this.cbEUR.UseVisualStyleBackColor = true;
            // 
            // cbPAC
            // 
            this.cbPAC.AutoSize = true;
            this.cbPAC.Location = new System.Drawing.Point(183, 12);
            this.cbPAC.Name = "cbPAC";
            this.cbPAC.Size = new System.Drawing.Size(47, 17);
            this.cbPAC.TabIndex = 6;
            this.cbPAC.Text = "PAC";
            this.cbPAC.UseVisualStyleBackColor = true;
            // 
            // cbLUB
            // 
            this.cbLUB.AutoSize = true;
            this.cbLUB.Location = new System.Drawing.Point(237, 12);
            this.cbLUB.Name = "cbLUB";
            this.cbLUB.Size = new System.Drawing.Size(47, 17);
            this.cbLUB.TabIndex = 7;
            this.cbLUB.Text = "LUB";
            this.cbLUB.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 125);
            this.Controls.Add(this.cbLUB);
            this.Controls.Add(this.cbPAC);
            this.Controls.Add(this.cbEUR);
            this.Controls.Add(this.cbNSA);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Presentation Document Crawler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.CheckBox cbNSA;
        private System.Windows.Forms.CheckBox cbEUR;
        private System.Windows.Forms.CheckBox cbPAC;
        private System.Windows.Forms.CheckBox cbLUB;
    }
}

