﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using System.Diagnostics;

namespace WordDocCrawler
{
    public partial class Form1 : Form
    {
        private string StudyYear = Properties.Settings.Default.Year;
        private bool NSA = Properties.Settings.Default.NSA;
        private bool EUR = Properties.Settings.Default.EUR;
        private bool PAC = Properties.Settings.Default.PAC;
        private bool LUB = Properties.Settings.Default.LUB;

        private Object missing = System.Type.Missing;
        List<string> Studies = new List<string>();
        string CorrDir = @"K:\STUDY\Refining\2012\*STUDY*\Correspondence\";
        public Form1()
        {
            InitializeComponent();
            cbEUR.Checked = EUR;
            cbNSA.Checked = NSA;
            cbPAC.Checked = PAC;
            cbLUB.Checked = LUB;
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            string ReportName = "";
            if (cbNSA.Checked) Studies.Add("NSA");
            if (cbEUR.Checked) Studies.Add("EUR");
            if (cbPAC.Checked) Studies.Add("PAC");
            if (cbLUB.Checked) Studies.Add("LUB");

         
            foreach (string study in Studies)
            {
               ReportName = "C:\\Presenter Files Double Check" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt";
                
                StreamWriter objWriter = new StreamWriter(ReportName, false);
                objWriter.WriteLine(study + " " + StudyYear);
                objWriter.WriteLine("------------------------------------------------------");
                string NewCorrDir = CorrDir.Replace("*STUDY*", study);
                string[] Dirs = Directory.GetDirectories(NewCorrDir);
                string search = "";
                Word.Document doc=null;
                foreach (string dir in Dirs)
                {
                    KillProcesses("WinWord");

                   string[] files = System.IO.Directory.GetFiles(dir + @"\", @"PN_*.*", System.IO.SearchOption.TopDirectoryOnly);

                   if (files.Length > 0)
                   {
                       if (files.Length > 1)
                       {
                           objWriter.WriteLine("MULTIPLE:  " + dir);
                       }
                       else
                       {
                           
                           int span = files[0].LastIndexOf(".")-files[0].LastIndexOf("PN_");

                           search = files[0].Substring(files[0].LastIndexOf("PN_")+3, span - 3);

                           
                           Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
                           wordApp.Visible = false;
                           wordApp.Caption = Guid.NewGuid().ToString();
                           try
                           {
                               doc = wordApp.Documents.Open(dir + "\\PN_" + search + ".doc", false, true, false, "whatever", missing, missing, missing, missing, missing, missing, false, false, missing, true, missing);

                               Word.Range rgFound = doc.Content;
                               Word.Range rgFound2 = doc.Content;

                               rgFound.Find.ClearFormatting();
                               rgFound2.Find.ClearFormatting();

                               rgFound.Find.Execute(search, false, true, false, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
                               rgFound2.Find.Execute(search.Replace(" ",""), false, true, false, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);

                               if (rgFound != null)
                               {
                                   if (rgFound.Find.Found || rgFound2.Find.Found)
                                   {
                                       txtFile.Text = search;
                                       objWriter.WriteLine("FOUND:     " + files[0].ToString());
                                   }
                                   else
                                   {
                                       txtFile.Text = search;
                                       objWriter.WriteLine("NOT FOUND: " + files[0].ToString());
                                   }
                               }
                               ((Word._Document)doc).Close(false, missing, missing);
                               doc = null;
                           }
                           catch
                           {
                               txtFile.Text = search;
                               objWriter.WriteLine("MISSING:   " + files[0].ToString());
                           }
                   

                            }
                   }

                }
                objWriter.WriteLine("------------------------------------------------------");
            }
        

        Process.Start(ReportName);
        }

        

        private void KillProcesses(string p)
        {
        Process[] pProcess  = System.Diagnostics.Process.GetProcessesByName(p);

        foreach (Process ep in pProcess)
            try
            {
                ep.Kill();
            }
            catch
            {
            }
            
        }


        }
    }

