﻿namespace QA
{
    partial class Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.btnSaveContent = new System.Windows.Forms.Button();
            this.lblEditType = new System.Windows.Forms.Label();
            this.lblContentID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCategory = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.cboConsultant = new System.Windows.Forms.ComboBox();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.Label60 = new System.Windows.Forms.Label();
            this.cboRefNum = new System.Windows.Forms.ComboBox();
            this.Label59 = new System.Windows.Forms.Label();
            this.cboStudy = new System.Windows.Forms.ComboBox();
            this.txtAnswer = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtQuestion
            // 
            this.txtQuestion.Location = new System.Drawing.Point(18, 136);
            this.txtQuestion.Multiline = true;
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtQuestion.Size = new System.Drawing.Size(771, 233);
            this.txtQuestion.TabIndex = 0;
            this.txtQuestion.TextChanged += new System.EventHandler(this.txtContent_TextChanged);
            // 
            // btnSaveContent
            // 
            this.btnSaveContent.Enabled = false;
            this.btnSaveContent.Location = new System.Drawing.Point(714, 631);
            this.btnSaveContent.Name = "btnSaveContent";
            this.btnSaveContent.Size = new System.Drawing.Size(75, 30);
            this.btnSaveContent.TabIndex = 118;
            this.btnSaveContent.Text = "Save";
            this.btnSaveContent.UseVisualStyleBackColor = true;
            this.btnSaveContent.Click += new System.EventHandler(this.btnSaveQuestion_Click);
            // 
            // lblEditType
            // 
            this.lblEditType.AutoSize = true;
            this.lblEditType.Location = new System.Drawing.Point(15, 120);
            this.lblEditType.Name = "lblEditType";
            this.lblEditType.Size = new System.Drawing.Size(35, 13);
            this.lblEditType.TabIndex = 119;
            this.lblEditType.Text = "label1";
            // 
            // lblContentID
            // 
            this.lblContentID.AutoSize = true;
            this.lblContentID.Location = new System.Drawing.Point(737, 664);
            this.lblContentID.Name = "lblContentID";
            this.lblContentID.Size = new System.Drawing.Size(35, 13);
            this.lblContentID.TabIndex = 120;
            this.lblContentID.Text = "label1";
            this.lblContentID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(15, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 141;
            this.label1.Text = "Category:";
            // 
            // cboCategory
            // 
            this.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.Items.AddRange(new object[] {
            "ALL",
            "Refinery History",
            "Study Boundary (Table 1)",
            "Material Balance",
            "Energy",
            "Personnel",
            "Maintenance",
            "Operating Expenses",
            "Process Data (Table 2)",
            "Miscellaneous",
            "Pricing",
            "Results Presentation"});
            this.cboCategory.Location = new System.Drawing.Point(100, 12);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(216, 24);
            this.cboCategory.TabIndex = 140;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(408, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 138;
            this.label3.Text = "(Separated by Commas)";
            // 
            // txtKeywords
            // 
            this.txtKeywords.Location = new System.Drawing.Point(411, 73);
            this.txtKeywords.Multiline = true;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(378, 33);
            this.txtKeywords.TabIndex = 137;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(322, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 136;
            this.label2.Text = "Keywords:";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Enabled = false;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(321, 14);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(90, 17);
            this.Label8.TabIndex = 135;
            this.Label8.Text = "Consultant:";
            // 
            // cboConsultant
            // 
            this.cboConsultant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConsultant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboConsultant.FormattingEnabled = true;
            this.cboConsultant.Location = new System.Drawing.Point(411, 13);
            this.cboConsultant.MaxLength = 3;
            this.cboConsultant.Name = "cboConsultant";
            this.cboConsultant.Size = new System.Drawing.Size(276, 24);
            this.cboConsultant.TabIndex = 134;
            // 
            // cboCompany
            // 
            this.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(411, 45);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(276, 24);
            this.cboCompany.Sorted = true;
            this.cboCompany.TabIndex = 133;
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Enabled = false;
            this.lblCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.Location = new System.Drawing.Point(330, 45);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(74, 17);
            this.lblCompany.TabIndex = 132;
            this.lblCompany.Text = "Refinery:";
            // 
            // Label60
            // 
            this.Label60.AutoSize = true;
            this.Label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label60.Location = new System.Drawing.Point(15, 80);
            this.Label60.Name = "Label60";
            this.Label60.Size = new System.Drawing.Size(70, 17);
            this.Label60.TabIndex = 131;
            this.Label60.Text = "RefNum:";
            // 
            // cboRefNum
            // 
            this.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRefNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRefNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRefNum.FormattingEnabled = true;
            this.cboRefNum.Location = new System.Drawing.Point(100, 78);
            this.cboRefNum.Name = "cboRefNum";
            this.cboRefNum.Size = new System.Drawing.Size(216, 24);
            this.cboRefNum.TabIndex = 130;
            // 
            // Label59
            // 
            this.Label59.AutoSize = true;
            this.Label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label59.Location = new System.Drawing.Point(15, 45);
            this.Label59.Name = "Label59";
            this.Label59.Size = new System.Drawing.Size(54, 17);
            this.Label59.TabIndex = 129;
            this.Label59.Text = "Study:";
            // 
            // cboStudy
            // 
            this.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStudy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStudy.FormattingEnabled = true;
            this.cboStudy.Items.AddRange(new object[] {
            "ALL",
            "NSA14",
            "EUR14",
            "PAC14",
            "LUB14",
            "NSA12",
            "EUR12",
            "PAC12",
            "LUB12"});
            this.cboStudy.Location = new System.Drawing.Point(100, 45);
            this.cboStudy.Name = "cboStudy";
            this.cboStudy.Size = new System.Drawing.Size(216, 24);
            this.cboStudy.TabIndex = 128;
            // 
            // txtAnswer
            // 
            this.txtAnswer.Location = new System.Drawing.Point(18, 387);
            this.txtAnswer.Multiline = true;
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAnswer.Size = new System.Drawing.Size(771, 219);
            this.txtAnswer.TabIndex = 142;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(714, 14);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 143;
            this.btnRefresh.Text = "Reset";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 692);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.txtAnswer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboCategory);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtKeywords);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.cboConsultant);
            this.Controls.Add(this.cboCompany);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.Label60);
            this.Controls.Add(this.cboRefNum);
            this.Controls.Add(this.Label59);
            this.Controls.Add(this.cboStudy);
            this.Controls.Add(this.lblContentID);
            this.Controls.Add(this.lblEditType);
            this.Controls.Add(this.btnSaveContent);
            this.Controls.Add(this.txtQuestion);
            this.Name = "Edit";
            this.Text = "Edit";
            this.Load += new System.EventHandler(this.Edit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.Button btnSaveContent;
        private System.Windows.Forms.Label lblEditType;
        private System.Windows.Forms.Label lblContentID;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox cboCategory;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtKeywords;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.ComboBox cboConsultant;
        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.Label lblCompany;
        internal System.Windows.Forms.Label Label60;
        internal System.Windows.Forms.ComboBox cboRefNum;
        internal System.Windows.Forms.Label Label59;
        internal System.Windows.Forms.ComboBox cboStudy;
        private System.Windows.Forms.TextBox txtAnswer;
        private System.Windows.Forms.Button btnRefresh;
    }
}