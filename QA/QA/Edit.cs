﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using sa.Internal.Console.DataObject;

namespace QA
{
    public partial class Edit : Form
    {
       SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
        
        sa.Internal.Console.DataObject.DataObject db = new sa.Internal.Console.DataObject.DataObject(sa.Internal.Console.DataObject.DataObject.StudyTypes.REFINING,"mgv","solomon2012");
        int QuestionID;
        int AnswerID;
        string EditMode;
        private string _RefNum;
        private string _Study;
        private string _Category;
        private string _Keywords;
        private string _Consultant;
        private string StudyYear;
        private string Study;

        

       
        
       
        public Edit(string editMode, string EditType, int questionID, int answerID, string question, string answer, string refNum, string study, string category, string keywords, string consultant)
        {
            InitializeComponent();
            lblEditType.Text = EditType;
            txtQuestion.Text = question;
            txtAnswer.Text = answer;
            AnswerID = answerID;
            QuestionID = questionID;
            EditMode = editMode;
            _RefNum = refNum;
            _Study = study;
            _Category = category;
            _Keywords = keywords;
            _Consultant = consultant;
            
            cboCategory.SelectedIndex = cboCategory.FindString(category);
            cboStudy.SelectedIndex = cboStudy.FindString(study);
            GetRefNums();
            
            GetConsultants();
            cboRefNum.SelectedIndex = cboRefNum.FindString(refNum);
            cboConsultant.SelectedIndex = cboConsultant.FindString(consultant);
            txtKeywords.Text = keywords;
            GetCompanyByRefNum();

        }


        private void GetRefNums()
        {
            cboRefNum.Items.Clear();
            if (cboStudy.Text == "ALL")
            {
                cboRefNum.Items.Add("ALL");
                cboCompany.Items.Add("ALL");
                cboRefNum.SelectedIndex = 0;
                cboCompany.SelectedIndex = 0;
                return;
            }
                
                
           
                StudyYear = "20" + cboStudy.Text.Substring(3, 2);
                Study = cboStudy.Text.Substring(0, 3);
                cboRefNum.Items.Add("ALL");
                cboCompany.Items.Add("ALL");
                DataSet ds;
                List<string> p = new List<string>();

                p.Add("StudyYear/" + StudyYear);
                p.Add("Study/" + Study);

                ds = db.ExecuteStoredProc("Console.GetRefNums", p);
                
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    cboRefNum.Items.Add(row["RefNum"].ToString().Trim());
                    cboCompany.Items.Add(row["CoLoc"].ToString().Trim());
                    
                }

            
        }


        private void GetCompanyByRefNum()
        {
            if (cboRefNum.Text != "ALL")
            {
                DataSet ds;
                List<string> p = new List<string>();

                p.Add("RefNum/" + cboRefNum.Text);

                ds = db.ExecuteStoredProc("Console.GetCompanyName", p);
                if (ds.Tables.Count > 0)

                    if (ds.Tables[0].Rows.Count > 0)
                        cboCompany.SelectedIndex = cboCompany.FindString(ds.Tables[0].Rows[0]["Coloc"].ToString().Trim());

                   
            }
            else

            cboCompany.SelectedIndex=0;
            


        }

        private void btnSaveQuestion_Click(object sender, EventArgs e)
        {
            SaveContent();
            if (System.Windows.Forms.Application.OpenForms["frmAdmin"] != null)
            {
                if (lblEditType.Text == "Question") 
                    (System.Windows.Forms.Application.OpenForms["frmAdmin"] as frmAdmin).GetQuestions(cboRefNum.Text,cboStudy.Text,cboCategory.Text,cboConsultant.Text,txtKeywords.Text);
                else
                    (System.Windows.Forms.Application.OpenForms["frmAdmin"] as frmAdmin).GetAnswers(QuestionID);

            }
            this.Close();
        }

        private void SaveContent()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cn.Open();
            if (EditMode == "Edit")
            {
                
                cmd.CommandType = CommandType.StoredProcedure;
                if (lblEditType.Text == "Question")
                {
                    cmd.CommandText = "UpdateQuestion";
                    cmd.Parameters.AddWithValue("@Content", txtQuestion.Text);
                    cmd.Parameters.AddWithValue("@ContentID", QuestionID);
                    cmd.Parameters.AddWithValue("@Study", cboStudy.Text);
                    cmd.Parameters.AddWithValue("@RefNum", cboRefNum.Text);
                    cmd.Parameters.AddWithValue("@Consultant", cboConsultant.Text);
                    cmd.Parameters.AddWithValue("@Category", cboCategory.Text);
                    cmd.Parameters.AddWithValue("@KeyWords", txtKeywords.Text);
                    
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    cmd.CommandText = "UpdateAnswer";
                    cmd.Parameters.AddWithValue("@Content", txtQuestion.Text);
                    cmd.Parameters.AddWithValue("@ContentID", AnswerID);
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {

                cmd.CommandType = CommandType.StoredProcedure;
                if (lblEditType.Text == "Question")
                {
                    cmd.CommandText = "InsertQuestion";
                    cmd.Parameters.AddWithValue("@Content", txtQuestion.Text);
                    cmd.Parameters.AddWithValue("@RefNum", _RefNum);
                    cmd.Parameters.AddWithValue("@Study", _Study);
                    cmd.Parameters.AddWithValue("@Category", _Category);
                    cmd.Parameters.AddWithValue("@Keywords", _Keywords);
                    cmd.Parameters.AddWithValue("@Consultant", _Consultant);
                    cmd.ExecuteNonQuery();
                    
                }
                else
                {
                    cmd.CommandText = "InsertAnswer";
                    cmd.Parameters.AddWithValue("@Content", txtAnswer.Text);
                    cmd.Parameters.AddWithValue("@ContentID", QuestionID);
                    cmd.ExecuteNonQuery();
                }


                cn.Close();

            }

            
        }



        private void txtContent_TextChanged(object sender, EventArgs e)
        {
            if ((txtQuestion.Text.Length > 1) || (txtAnswer.Text.Length > 1))
            btnSaveContent.Enabled = true;
        }

        private void Edit_Load(object sender, EventArgs e)
        {
            if (lblEditType.Text == "Question")
            {
                txtQuestion.ReadOnly = false;
                txtAnswer.ReadOnly = true;
            }
            else
            {
                txtQuestion.ReadOnly = true;
                txtAnswer.ReadOnly = false;
                

            }
        }

        private void btnRefresh_Click(object sender, EventArgs d)
        {

            cboCategory.SelectedIndex = 0;
            cboStudy.SelectedIndex = 0;
            cboRefNum.SelectedIndex=0;
            cboCompany.SelectedIndex = 0;
            cboConsultant.SelectedIndex = 0;
            txtKeywords.Text = "";
        }


        private void GetConsultants()
        {
            DataSet ds;

            db = new sa.Internal.Console.DataObject.DataObject(sa.Internal.Console.DataObject.DataObject.StudyTypes.REFINING, "mgv", "solomon2012");

            ds = db.ExecuteStoredProc("Console.GetAllConsultants");
            cboConsultant.Items.Add("ALL");
            if (ds.Tables.Count > 0)
                foreach (DataRow Row in ds.Tables[0].Rows)
                    cboConsultant.Items.Add(Row["Consultant"].ToString().ToUpper().Trim() + " - " + Row["ConsultantName"].ToString().ToUpper().Trim());
        }
    }
}
