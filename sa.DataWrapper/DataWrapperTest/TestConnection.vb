﻿Imports System.Text
Imports sa.DataWrapper


<TestClass()>
Public Class TestConnection
    Dim params As List(Of String)

    <TestMethod()>
    Public Sub TestRefiningDatabaseConnection()
        params = New List(Of String)
        params.Add("Consultant/DBB")
        params.Add("StudyYear/2012")
        params.Add("Study/Fuels")
        Dim da As New DataWrapper(DataWrapper.StudyTypes.REFINING, "mgv", "solomon2012")
        Dim ds As DataSet
        ds = da.ExecuteStoredProc("Console.GetRefNumsByConsultant", params)
        Assert.AreNotEqual(ds.Tables.Count, 0)
    End Sub
    <TestMethod()>
    Public Sub TestRAMConnection()
        params = New List(Of String)
        params.Add("StudyYear/2012")
        params.Add("Mode/0")

        Dim da As New DataWrapper(DataWrapper.StudyTypes.RAM, "mgv", "solomon2012")
        Dim ds As DataSet
        ds = da.ExecuteStoredProc("Console.GetRefNums", params)
        Assert.AreNotEqual(ds.Tables.Count, 0)   'Run in SQL Manager to verify correct value
    End Sub
    <TestMethod()>
    Public Sub TestOlefinsDatabaseConnection()
        Dim da As New DataWrapper(DataWrapper.StudyTypes.OLEFINS, "mgv", "solomon2012")
        Dim ds As DataSet
        ds = da.ExecuteStoredProc("Console.GetAllConsultants")
        Assert.AreNotEqual(ds.Tables.Count, 0)

    End Sub

    <TestMethod()>
    Public Sub TestPowerDatabaseConnection()
        Dim da As New DataWrapper(DataWrapper.StudyTypes.POWER, "mgv", "solomon2012")
        Dim ds As DataSet
        ds = da.ExecuteStoredProc("Console.GetAllConsultants")
        Assert.AreNotEqual(ds.Tables.Count, 0)

    End Sub

    
End Class
