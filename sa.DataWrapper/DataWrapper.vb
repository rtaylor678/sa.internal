﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Collections

Public Class DataWrapper
    Enum StudyTypes
        REFINING
        OLEFINS
        POWER
        RAM
        PIPELINES
        REFINING_GLOBAL
    End Enum
    Private conn As SqlConnection

    Private study As String
    Private mStudyType As StudyTypes
    Public UserName As String
    Public Password As String
    Private mConn As String
    Private mError As String
    Public Property DBError() As String
        Get
            Return mError
        End Get
        Set(ByVal value As String)
            mError = value
        End Set
    End Property

    Public Property ConnectionString() As String
        Get
            Return mConn
        End Get
        Set(ByVal value As String)
            mConn = value
        End Set
    End Property

    Public Property StudyType() As StudyTypes
        Get
            Return mStudyType
        End Get
        Set(ByVal value As StudyTypes)
            mStudyType = value
        End Set
    End Property

    Private mStudyYear As String
    Public Property StudyYear() As String
        Get
            Return mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property
    Private _DBCommand As String
    Public Property DBCommand() As String
        Get
            Return _DBCommand
        End Get
        Set(ByVal value As String)
            _DBCommand = value
        End Set
    End Property

    Private _SQL As String
    Public Property SQL() As String
        Get
            Return _SQL
        End Get
        Set(ByVal value As String)
            _SQL = value
        End Set
    End Property


    Public Sub New(studyType As StudyTypes, UserName As String, Password As String)
        mStudyType = studyType

        Select Case mStudyType
            Case StudyTypes.REFINING
                study = "REFINING"
            Case StudyTypes.OLEFINS
                study = "OLEFINS"
            Case StudyTypes.POWER
                study = "POWER"
            Case StudyTypes.PIPELINES
                study = "PIPELINES"
            Case StudyTypes.RAM
                study = "RAM"
            Case StudyTypes.REFINING_GLOBAL
                study = "REFINING_GLOBAL"
        End Select



        ConnectionString = My.Settings.Item(study) & "User Id=" & UserName & ";Password=" & Password & ";"
        Dim sqlconn As New SqlConnection(ConnectionString)
        Try
            sqlconn.Open()
        Catch ex As Exception
            DBError = ex.Message
        End Try
        ConnectionString = My.Settings.Item(study) & "User Id=MGV;Password=solomon2012;"
        sqlconn.Close()
    End Sub

    Public Function IsValid(DataConnection As String) As Boolean

        Dim Valid As Boolean = False

        Dim conn As New SqlConnection(DataConnection)
        Try
            conn.Open()
            Valid = True

        Catch ex As System.Exception
            Valid = False
        End Try
        conn.Close()
        Return Valid

    End Function

    Public Function ExecuteReader(spName As String, Optional params As List(Of String) = Nothing) As SqlDataReader
        Dim rdr As SqlDataReader = Nothing
        Dim cmd As New SqlCommand()
        Try

            Dim conn As New SqlConnection(ConnectionString)
            cmd.CommandText = spName
            If Not params Is Nothing Then

                Dim pm() As String
                Dim p As String

                For Each p In params
                    pm = p.Split("/")
                    cmd.Parameters.AddWithValue(pm(0), pm(1))
                Next

            End If

            cmd.CommandType = CommandType.StoredProcedure
            conn.Open()
            cmd.Connection = conn
            rdr = cmd.ExecuteReader()
        Catch Ex As System.Exception

        End Try

        Return rdr
    End Function
    Public Function ExecuteReader() As SqlDataReader

        Dim rdr As SqlDataReader = Nothing
        Dim cmd As New SqlCommand(SQL)
        Dim conn As New SqlConnection(connectionstring)
        conn.Open()
        cmd.Connection = conn
        Try
            rdr = cmd.ExecuteReader
        Catch
            conn.Close()
        End Try
        Return rdr

    End Function

    Public Function Execute() As DataSet

        Dim conn As New SqlConnection(connectionstring)
        Dim ds As New DataSet("Studies")
        Dim da As New SqlDataAdapter()
        Dim cmd As New SqlCommand(SQL)
        da.SelectCommand = cmd
        cmd.CommandType = CommandType.Text

        conn.Open()
        cmd.Connection = conn
        Try
            da.Fill(ds)
        Catch
            conn.Close()
        End Try
        Return ds

    End Function
    Public Function ExecuteNonQuery(spName As String, Optional params As List(Of String) = Nothing) As Integer


        Dim cmd As New SqlCommand()
        Dim conn As New SqlConnection(connectionstring)
        cmd.Connection = conn
        cmd.CommandText = spName
        If Not params Is Nothing Then

            Dim pm() As String
            Dim p As String

            For Each p In params
                pm = p.Split("/")
                cmd.Parameters.AddWithValue(pm(0), pm(1))
            Next
        End If

        cmd.CommandType = CommandType.StoredProcedure
        conn.Open()
        cmd.Connection = conn
        Try
            Dim c As Integer = cmd.ExecuteNonQuery()

            conn.Close()
            Return c
        Catch Ex As System.Exception

            Return 0
        End Try

    End Function
    Public Function ExecuteNonQuery() As Integer

        Dim count As Integer = 0
        Dim cmd As New SqlCommand(SQL)
        Dim conn As New SqlConnection(connectionstring)
        cmd.Connection = conn
        conn.Open()
        Try
            count = cmd.ExecuteNonQuery
        Catch
        End Try
        conn.Close()
        Return count

    End Function

    Public Function ExecuteStoredProc(spName As String, Optional params As List(Of String) = Nothing) As DataSet

        Dim ds As New DataSet("Studies")
        Dim da As New SqlDataAdapter()
        Try
            Dim cmd As New SqlCommand(SQL)
            Dim conn As New SqlConnection(connectionstring)
            cmd.Connection = conn
            cmd.CommandText = spName
            If Not params Is Nothing Then

                Dim pm() As String
                Dim p As String

                For Each p In params
                    pm = p.Split("/")
                    cmd.Parameters.AddWithValue(pm(0), pm(1))
                Next
            End If

            cmd.CommandType = CommandType.StoredProcedure
            conn.Open()
            cmd.Connection = conn
            da.SelectCommand = cmd
            da.Fill(ds)

            conn.Close()
        Catch Ex As System.Exception

        End Try

        Return ds

    End Function



End Class
