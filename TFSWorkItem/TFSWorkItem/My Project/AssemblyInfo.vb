﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("TFSWorkItem")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("HSB Solomon Associates LLC")> 
<Assembly: AssemblyProduct("TFSWorkItem")> 
<Assembly: AssemblyCopyright("© 2015 HSB Solomon Associates LLC")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("bf28f943-3a84-4ce5-8aad-60a1cf705a2b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2015.11.5.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
