﻿Imports System.Web
Imports System.Net.Mail
Imports System.Collections
Imports Microsoft.TeamFoundation.Client
Imports Microsoft.TeamFoundation.Framework.Common
Imports Microsoft.TeamFoundation.Framework.Client
Imports Microsoft.TeamFoundation.WorkItemTracking.Client

Public Class TFSWorkItem

    Private _server As String
    Public Property Server() As String
        Get
            Return _server
        End Get
        Set(ByVal value As String)
            _server = value
        End Set
    End Property

    Private _priority As String
    Public Property Priority() As String
        Get
            Return _priority
        End Get
        Set(ByVal value As String)
            _priority = value
        End Set
    End Property

    Private _assignedTo As String
    Public Property AssignedTo() As String
        Get
            Return _assignedTo
        End Get
        Set(ByVal value As String)
            _assignedTo = value
        End Set
    End Property


    Private _workItemText As String
    Public Property WorkItemText() As String
        Get
            Return _workItemText
        End Get
        Set(ByVal value As String)
            _workItemText = value
        End Set
    End Property


    Private _title As String
    Public Property Title() As String
        Get
            Return _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Private _workItemType As String
    Public Property WorkItemType() As String
        Get
            Return _workItemType
        End Get
        Set(ByVal value As String)
            _workItemType = value
        End Set
    End Property

    Private _project As String
    Public Property Project() As String
        Get
            Return _project
        End Get
        Set(ByVal value As String)
            _project = value
        End Set
    End Property
    Private _username As String
    Public ReadOnly Property CreatedBy() As String
        Get
            Return _username
        End Get

    End Property

    Public Sub New(Optional server As String = "", Optional project As String = "", Optional workitemtype As String = "", Optional priority As String = "", Optional assignedto As String = "", Optional workitem As String = "")
        _server = server
        _workItemType = workitemtype
        _workItemText = workitem
        _priority = priority
        _project = project
    End Sub

    Public Function InsertWorkItemOrig(Optional attachmentPaths As List(Of String) = Nothing) As String
        Dim userStory As Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItem
        Dim collectionUri As Uri
        If _server.Length > 10 And _project.Length > 2 Then
            Try
                collectionUri = New Uri(_server)
                Dim tpc As New TfsTeamProjectCollection(collectionUri)
                Dim workItemStore As WorkItemStore
                workItemStore = tpc.GetService(Of WorkItemStore)()
                Dim teamProject As Project
                teamProject = workItemStore.Projects(_project)
                Dim mWorkItemType As WorkItemType

                'Dim areaRootNodes As Microsoft.TeamFoundation.WorkItemTracking.Client.NodeCollection
                'areaRootNodes = teamProject.AreaRootNodes

                Dim wIType As WorkItemType = teamProject.WorkItemTypes(WorkItemType)
                Dim workItem As WorkItem = New WorkItem(wIType)

                Dim area = teamProject.AreaRootNodes("Console").Id
                workItem.Title = WorkItemText
                workItem.Description = WorkItemText
                workItem.AreaId = teamProject.AreaRootNodes("Console").Id
                'workItem.Fields("Assigned To").Value = strAssUser
                'workItem.Fields("System Info").Value = "Access V 1.1.25"
                'workItem.Fields("Repro Steps").Value = Desc

                Dim result As ArrayList = workItem.Validate()

                If result.Count > 0 Then
                    Return (result(0).ToString + "There was an error adding this work item to the work item repository")
                Else
                    workItem.Save()
                End If







                'need to fix the attaching part too







                Exit Function

                'For Each areaRootNode As Microsoft.TeamFoundation.WorkItemTracking.Client.Node In areaRootNodes
                '    If areaRootNode.Name = "Console" Then




                '        Exit For
                '    End If
                'Next





                mWorkItemType = teamProject.WorkItemTypes(_workItemType)
                userStory = New Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItem(mWorkItemType)

                If _workItemType.ToUpper = "TASK" Then
                    userStory.Title = "Console Request"
                    userStory.Description = _workItemText
                Else
                    userStory.Title = "Console Bug"
                    userStory.Fields("Microsoft.VSTS.TCM.ReproSteps").Value = _workItemText
                End If

                Dim pathToAttachment As String = String.Empty
                Try
                    If Not IsNothing(attachmentPaths) Then
                        For Each path As String In attachmentPaths
                            pathToAttachment = pathToAttachment
                            Dim attachment As New Microsoft.TeamFoundation.WorkItemTracking.Client.Attachment(path)
                            userStory.Attachments.Add(attachment)
                        Next
                    End If
                Catch ex As Exception
                    MsgBox("Problem attaching file at " + pathToAttachment)
                End Try

                ' Save the new user story
                userStory.Save()
                Return "1"
            Catch ex As Exception
                Return ex.Message
            End Try
        Else
            Return "Missing one of the following required parameters:  Server or Project"
        End If
    End Function

    Public Function InsertWorkItem(projectCollectionUrl As String,
                                   projectName As String,
                                   workItemType As String,
                                   title As String,
                                   description As String,
                                   iterationPath As String,
                                   areaPath As String,
                                   productBacklogId As Integer,
                                    Optional attachmentPaths As List(Of String) = Nothing) As Boolean

        If title.Trim().Length < 1 Then
            Throw New Exception("TFS Item title must not be blank")
        End If
        If projectCollectionUrl.Length > 10 And projectName.Length > 2 Then
            Try
                Dim tfs As TeamFoundationServer = TeamFoundationServerFactory.GetServer(projectCollectionUrl)
                Dim store As WorkItemStore = tfs.GetService(Of WorkItemStore)()
                Dim wiType1 As WorkItemType = store.Projects(projectName).WorkItemTypes("Task") 'Can't do as bug right now because of the way we have Data Management set up now.

                Dim newWI As WorkItem = New WorkItem(wiType1)
                newWI.Title = title
                newWI.Description = description
                newWI.State = "Planning"
                'newWI.Fields("System.AssignedTo").Value = "Stuart F. Bard"
                newWI.IterationPath = iterationPath '` "Data Management\2016\6-20 ~ 6-24"
                newWI.AreaPath = areaPath ' "Data Management\Console"

                If Not IsNothing(attachmentPaths) Then
                    For Each path As String In attachmentPaths
                        Try
                            Dim attachment As New Microsoft.TeamFoundation.WorkItemTracking.Client.Attachment(path)
                            'userStory.Attachments.Add(attachment)
                            newWI.Attachments.Add(attachment)
                        Catch exAttach As Exception
                            MsgBox("Problem attaching file at " + path)
                        End Try
                    Next
                End If
                Dim errorsList As ArrayList = newWI.Validate()
                If errorsList.Count > 0 Then
                    For count As Integer = 0 To errorsList.Count - 1
                        MsgBox("TFS Item error: " + errorsList(count).Name)
                    Next
                    Return False
                End If
                newWI.Save()
                Dim taskId As Integer = newWI.Id  '363
                Dim linkType = store.WorkItemLinkTypes("System.LinkTypes.Hierarchy")
                Dim taskLink As New WorkItemLink(linkType.ForwardEnd, taskId)
                Dim backlogItem = store.GetWorkItem(productBacklogId) '334)  'prductBacklogId
                backlogItem.Links.Add(taskLink)
                backlogItem.Save()
                Return True
            Catch ex As Exception
                MsgBox("Tfs Item error: " + ex.Message)
                Return False
            End Try
        End If

    End Function




    Public Function InsertWorkItemTest(Optional attachmentPaths As List(Of String) = Nothing) As String
        Dim userStory As Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItem
        Dim collectionUri As Uri
        If _server.Length > 10 And _project.Length > 2 Then
            Try

                '--------------------------
                '_server =  "http://dbs7:8080/tfs/CPA_20130129"
                '_project = "Data Management"
                'work item tyep  task/bug etc.
                'title
                'descriptoin
                'iteratino path  
                'area path
                'prductBacklogId  334

                'found some code:
                Dim tfs As TeamFoundationServer = TeamFoundationServerFactory.GetServer(_server)
                Dim store As WorkItemStore = tfs.GetService(Of WorkItemStore)()
                Dim wiType1 As WorkItemType = store.Projects("Data Management").WorkItemTypes("Task")

                Dim newWI As WorkItem = New WorkItem(wiType1)
                newWI.Title = "Automation Test 5"
                newWI.State = "Planning"
                newWI.Fields("System.AssignedTo").Value = "Stuart F. Bard"
                newWI.IterationPath = "Data Management\2016\6-20 ~ 6-24"
                newWI.AreaPath = "Data Management\Console"

                'need to add a parent link to 334
                Dim probls As ArrayList = newWI.Validate()
                newWI.Save()

                Dim taskId As Integer = newWI.Id  '363
                Dim linkType = store.WorkItemLinkTypes("System.LinkTypes.Hierarchy")
                Dim taskLink As New WorkItemLink(linkType.ForwardEnd, taskId)

                Dim backlogItem = store.GetWorkItem(334)  'prductBacklogId
                backlogItem.Links.Add(taskLink)
                backlogItem.Save()

                Exit Function
                '--------------------------


                collectionUri = New Uri(_server)
                Dim tpc As New TfsTeamProjectCollection(collectionUri)
                Dim workItemStore As WorkItemStore
                workItemStore = tpc.GetService(Of WorkItemStore)()
                Dim teamProject As Project
                teamProject = workItemStore.Projects(_project)
                'Dim mWorkItemType As WorkItemType

                'Dim areaRootNodes As Microsoft.TeamFoundation.WorkItemTracking.Client.NodeCollection
                'areaRootNodes = teamProject.AreaRootNodes
                Dim area = teamProject.AreaRootNodes("Console").Id



                Dim wIType As WorkItemType = teamProject.WorkItemTypes(WorkItemType)
                wIType = teamProject.WorkItemTypes("Product Backlog Item")

                Dim taskType As WorkItemType = teamProject.WorkItemTypes("Task")
                Dim wi As New WorkItem(taskType)

                'this finds existing task
                'Dim store As Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore = wIType.Store
                'wi = store.GetWorkItem(355)

                Dim newTask As New WorkItem(taskType)
                newTask.AreaId = 698
                newTask.AreaPath = "Data Management\Console"
                newTask.Description = "AutomationTest2"
                newTask.IterationPath = "Data Management\2016\6-20 ~ 6-24"
                'read only newTask.NodeName = "Console"
                'readonly newTask.Project = teamProject

                'newTask.Reason = "not done"
                newTask.State = "Planning"
                newTask.Title = "AutomationTest2"
                'newTask.Type=  811
                Dim problems As ArrayList = newTask.Validate()
                If problems.Count < 1 Then
                    newTask.Save()
                End If

                Exit Function


                Dim stophere As String = String.Empty
                'Product Backlog Item
                Dim workItem As WorkItem = New WorkItem(wIType)

                workItem.Title = WorkItemText
                workItem.Description = WorkItemText
                workItem.AreaId = teamProject.AreaRootNodes("Console").Id
                'workItem.Fields("Assigned To").Value = strAssUser
                'workItem.Fields("System Info").Value = "Access V 1.1.25"
                'workItem.Fields("Repro Steps").Value = Desc
                workItem.IterationPath = "Data Management\2016\6-20 ~ 6-24"


                Dim result As ArrayList = workItem.Validate()

                If result.Count > 0 Then
                    Return (result(0).ToString + "There was an error adding this work item to the work item repository")
                Else
                    workItem.Save()
                End If
            Catch ex As Exception
                Dim err As String = ex.Message
            End Try

        End If




        'need to fix the attaching part too



        Return String.Empty
    End Function


    Public Function InsertNewBacklogItem(Optional attachmentPaths As List(Of String) = Nothing) As String
        MsgBox("value of WorkItemText will be the name of the new backlog item.")
        Dim userStory As Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItem
        Dim collectionUri As Uri
        If _server.Length > 10 And _project.Length > 2 Then
            Try
                collectionUri = New Uri(_server)
                Dim tpc As New TfsTeamProjectCollection(collectionUri)
                Dim workItemStore As WorkItemStore
                workItemStore = tpc.GetService(Of WorkItemStore)()
                Dim teamProject As Project
                teamProject = workItemStore.Projects(_project)
                Dim mWorkItemType As WorkItemType

                'Dim areaRootNodes As Microsoft.TeamFoundation.WorkItemTracking.Client.NodeCollection
                'areaRootNodes = teamProject.AreaRootNodes

                Dim wIType As WorkItemType = teamProject.WorkItemTypes(WorkItemType)
                Dim workItem As WorkItem = New WorkItem(wIType)

                Dim area = teamProject.AreaRootNodes("Console").Id
                workItem.Title = WorkItemText
                workItem.Description = WorkItemText
                workItem.AreaId = teamProject.AreaRootNodes("Console").Id
                'workItem.Fields("Assigned To").Value = strAssUser
                'workItem.Fields("System Info").Value = "Access V 1.1.25"
                'workItem.Fields("Repro Steps").Value = Desc
                workItem.IterationPath = "Data Management\2016\6-20 ~ 6-24"


                Dim result As ArrayList = workItem.Validate()

                If result.Count > 0 Then
                    Return (result(0).ToString + "There was an error adding this work item to the work item repository")
                Else
                    workItem.Save()
                End If
            Catch ex As Exception
                Dim err As String = ex.Message
            End Try

        End If




        'need to fix the attaching part too



        Return String.Empty
    End Function

End Class


