﻿using System;
using System.Collections.Generic;
using System.Text;

using System.DirectoryServices;

namespace Sa.Internal
{
    /// <summary>
    /// 
    /// </summary>
    public class ActiveDirectory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="UserInitials"></param>
        /// <returns></returns>
        public static List<string> UserInfo(string username, string password, string UserInitials)
        {
            DirectoryEntry de = new DirectoryEntry("LDAP://dc1.solomononline.com", username, password, AuthenticationTypes.Secure);
            DirectorySearcher ds = new DirectorySearcher(de);

            ds.Filter = "(&(objectClass=user)((sAMAccountname=" + UserInitials + "))((mail=*.*)))";
            ds.SearchScope = SearchScope.Subtree;

            List<string> l = new List<string>();
            SearchResult r = ds.FindOne();

            if (r != null)
            {
                DirectoryEntry d = r.GetDirectoryEntry();

                l.Add((string)d.Properties["givenName"].Value);
                l.Add((string)d.Properties["displayName"].Value);
                l.Add((string)d.Properties["mail"].Value);

                return l;
            }
            else
            {
                return null;
            }
        }
    }
}
