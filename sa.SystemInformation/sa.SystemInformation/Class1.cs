﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Net;
using System.IO;
using Microsoft.Win32;
using System.Management;

namespace sa.SystemInformation
{
    
        public class NetworkInformation
        {
            private string _NetworkName;
            private string _DeviceName;
            private string _AdapterName;
            private string _MACAddress;
            private string _IPAddress;
            private string _SubnetMask;
            private string _DefaultGateway;
            private string _ConnectionStatus;
            private string _WebServiceStatus;
            private string _InternetStatus;


            public string NetworkName { get { return _NetworkName; } }
            public string DeviceName { get { return _DeviceName; } }
            public string AdapterName { get { return _AdapterName; } }
            public string MACAddress { get { return _MACAddress; } }
            public string IPAddress { get { return _IPAddress; } }
            public string SubnetMask { get { return _SubnetMask; } }
            public string DefaultGateway { get { return _DefaultGateway; } }
            public string ConnectionStatus { get { return _ConnectionStatus; } }
            public string WebServiceStatus { get { return _WebServiceStatus; } }
            public string InternetStatus { get { return _InternetStatus; } }

            public void GetNetworks()
            {

                NetworkManager mgr = new NetworkManager();
                NetworkInfo status = mgr.NetworkStatus();
                _NetworkName = status.ConnectionID;
                _DeviceName = status.DeviceName;
                _MACAddress = status.MacAddress;
                _IPAddress = status.IP;
                _SubnetMask = status.Mask;
                _DefaultGateway = status.DefaultGateway;
                _ConnectionStatus = status.GetHelp();


            }

            private string GetInternetStatus(string address)
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);



                    HttpWebResponse response;
                    request.Timeout = 5000;
                    request.Credentials = CredentialCache.DefaultNetworkCredentials;
                    response = (HttpWebResponse)request.GetResponse();
                    return "Connection: " + response.StatusCode;
                }
                catch (Exception err)
                {
                    return "Connection: " + err.Message;
                }
            }



            private string ServiceExists(
                            string address)
            {

                HttpWebResponse res = null;

                try
                {
                    // Create a request to the passed URI.  
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                    req.Credentials = CredentialCache.DefaultNetworkCredentials;
                    req.Timeout = 10000;
                    // Get the response object.  
                    res = (HttpWebResponse)req.GetResponse();

                    return "Service Up";
                }
                catch (Exception e)
                {

                    return "Host Unavailable: " + e.Message;
                }
            }

        }
        public class ComputerInformation
        {
            private string _ComputerName;
            private string _OperatingSystem;
            private string _Processor;
            private string _DotNetVersion;
            private string _Memory;

            public string ComputerName { get { return _ComputerName; } }
            public string OperatingSystem { get { return _OperatingSystem; } }
            public string Processor { get { return _Processor; } }
            public string Memory { get { return _Memory; } }
            public string DotNetVersion { get { return _DotNetVersion; } }




            public void GetComputerInformation()
            {
               
                _ComputerName = GetComputerName();
                _DotNetVersion = GetDotNetVersion();
                _Memory = GetRAM();
                _OperatingSystem = GetOperatingSystem();
                _Processor = GetProcessor();

            }



            private string GetDotNetVersion()
            {
                string CurrentVersion = "";
                string path = System.Environment.SystemDirectory;
                path = path.Substring(0, path.LastIndexOf('\\'));
                path = Path.Combine(path, "Microsoft.NET");
                // C:\WINDOWS\Microsoft.NET\

                string[] versions = new string[]{
                                            "Framework\\v1.0.3705",
                                            "Framework64\\v1.0.3705",
                                            "Framework\\v1.1.4322",
                                            "Framework64\\v1.1.4322",
                                            "Framework\\v2.0.50727",
                                            "Framework64\\v2.0.50727",
                                            "Framework\\v3.0",
                                            "Framework64\\v3.0",
                                            "Framework\\v3.5",
                                            "Framework64\\v3.5",
                                            "Framework\\v3.5\\Microsoft .NET Framework 3.5 SP1",
                                            "Framework64\\v3.5\\Microsoft .NET Framework 3.5 SP1",
                                            "Framework\\v4.0",
                                            "Framework64\\v4.0"
                                        };

                foreach (string version in versions)
                {
                    string versionPath = Path.Combine(path, version);

                    DirectoryInfo dir = new DirectoryInfo(versionPath);
                    if (dir.Exists)
                    {
                        CurrentVersion = version;
                    }

                }

                return CurrentVersion;

            }


            private string HKLM_GetString(string path, string key)
            {
                try
                {
                    RegistryKey rk = Registry.LocalMachine.OpenSubKey(path);
                    if (rk == null) return "";
                    return (string)rk.GetValue(key);
                }
                catch { return ""; }
            }

            private string FriendlyName()
            {
                string ProductName = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName");
                string CSDVersion = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CSDVersion");
                if (ProductName != "")
                {
                    return (ProductName.StartsWith("Microsoft") ? "" : "Microsoft ") + ProductName +
                                (CSDVersion != "" ? " " + CSDVersion : "");
                }
                return "";
            }


            private string GetComputerName()
            {
                return Environment.MachineName;
            }

            private string GetOperatingSystem()
            {
                return FriendlyName();
            }



            private string GetRAM()
            {
                return Convert.ToInt32(Convert.ToDouble(GetMemory()) / 1000000).ToString() + " MBytes";
            }



            private string GetProcessor()
            {
                RegistryKey processor_name = Registry.LocalMachine.OpenSubKey(@"Hardware\Description\System\CentralProcessor\0", RegistryKeyPermissionCheck.ReadSubTree);   //This registry entry contains entry for processor info.


                return processor_name.GetValue("ProcessorNameString").ToString();   //Display processor ingo.


            }

            private string GetMemory()
            {
                ObjectQuery winQuery = new ObjectQuery("SELECT TotalPhysicalMemory FROM Win32_ComputerSystem");

                ManagementObjectSearcher searcher = new ManagementObjectSearcher(winQuery);

                foreach (ManagementObject item in searcher.Get())
                {

                    return item["TotalPhysicalMemory"].ToString();


                }

                return null;


            }


        

    }
}
