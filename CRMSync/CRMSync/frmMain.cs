﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using sa.DataWrapper;
using System.Data.SqlClient;


namespace CRMSync
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ReadData();  
        }

        private void ReadData()
        {
            SqlConnection ConnCRM = new SqlConnection();
            SqlCommand cmdCRM = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            cmdCRM.Connection = ConnCRM;
            cmdCRM.CommandType = CommandType.Text;
            ConnCRM.ConnectionString = "Server=10.10.41.7;Database=DEVDynamics_MSCRM;User Id=mgv;Password=solomon2012;";
            ConnCRM.Open();
            sa.DataWrapper.DataWrapper dwConsole = new sa.DataWrapper.DataWrapper(DataWrapper.StudyTypes.REFINING, "mgv", "solomon2012");


            List<string> SQLs = new List<string>();
            SQLs.Add("SELECT * FROM ContactInfo where name is not null and substring(name,1,3) <> '???' order by name");
            SQLs.Add("SELECT * FROM CoContactInfo where contacttype = 'Coord' order by lastname");
            DataRow rowConsole;
            int sqlCount = 1;
            foreach (string sql in SQLs)
            {


                DataSet dsCONSOLE = new DataSet();
                dwConsole.SQL = sql;
                dsCONSOLE = dwConsole.Execute();
                if (sqlCount == 1)
                {
                    dgContacts.Columns.Add("LastName", "LastName");
                    dgContacts.Columns.Add("FirstName", "FirstName");
                    dgContacts.Columns.Add("Email", "Email");
                    dgContacts.Columns.Add("CustomerName", "CustomerName");
                }
                else
                {
                    dgCoContacts.Columns.Add("LastName", "LastName");
                    dgCoContacts.Columns.Add("FirstName", "FirstName");
                    dgCoContacts.Columns.Add("Email", "Email");
                    dgCoContacts.Columns.Add("CustomerName", "CustomerName");
                }
               
                try
                {
                    int rowcount = 0;
                    for (int i = 0; i < dsCONSOLE.Tables[0].Rows.Count - 1; i++)
                    {
                        
                        DataSet ds = new DataSet();
                        rowConsole = dsCONSOLE.Tables[0].Rows[i];

                        if (sqlCount == 1)
                        {


                            da = new SqlDataAdapter("SELECT LastName,FirstName, parentcustomeridname,emailaddress1 from ContactBase WHERE UPPER(emailaddress1) = '" + rowConsole["email"].ToString().Replace("'", "").Trim().ToUpper() + "';", ConnCRM);
                            ds = new DataSet();
                            da.Fill(ds, "Contacts");
                            
                            int row = ds.Tables["Contacts"].Rows.Count-1;
                            if (row >= 0 & RowDoesNotExist(dgContacts, ds))
                            {
                                dgContacts.Rows.Add();
                                dgContacts.Rows[rowcount].Cells[0].Value = ds.Tables["Contacts"].Rows[0].ItemArray[0].ToString().ToUpper().Trim();
                                dgContacts.Rows[rowcount].Cells[1].Value = ds.Tables["Contacts"].Rows[0].ItemArray[1].ToString().ToUpper().Trim();
                                dgContacts.Rows[rowcount].Cells[2].Value = ds.Tables["Contacts"].Rows[0].ItemArray[2].ToString().ToUpper().Trim();
                                dgContacts.Rows[rowcount].Cells[3].Value = ds.Tables["Contacts"].Rows[0].ItemArray[3].ToString().ToUpper().Trim();
                                rowcount++;
                            }
                    
                        
                      

                        }
                        else
                        {

                            da = new SqlDataAdapter("SELECT LastName ,FirstName, parentcustomeridname,emailaddress1 from ContactBase WHERE UPPER(emailaddress1) = '" + rowConsole["email"].ToString().Replace("'", "").Trim().ToUpper() + "';", ConnCRM);
                            ds = new DataSet();
                            da.Fill(ds, "Contacts");
                            
                            int row = ds.Tables["Contacts"].Rows.Count-1;
                            if (row >= 0 & RowDoesNotExist(dgCoContacts,ds))
                            {
                                dgCoContacts.Rows.Add();
                                dgCoContacts.Rows[rowcount].Cells[0].Value = ds.Tables["Contacts"].Rows[0].ItemArray[0].ToString().ToUpper().Trim();
                                dgCoContacts.Rows[rowcount].Cells[1].Value = ds.Tables["Contacts"].Rows[0].ItemArray[1].ToString().ToUpper().Trim();
                                dgCoContacts.Rows[rowcount].Cells[2].Value = ds.Tables["Contacts"].Rows[0].ItemArray[2].ToString().ToUpper().Trim();
                                dgCoContacts.Rows[rowcount].Cells[3].Value = ds.Tables["Contacts"].Rows[0].ItemArray[3].ToString().ToUpper().Trim();
                                rowcount++;
                            }
                        }


                       ConnCRM.Close();
                    }
                }
                catch { Exception er; };



                sqlCount++;
            }

            


            


        }
        private bool RowDoesNotExist(DataGridView dgv, DataSet DS)
        {
            bool DoesNotExists = true;
            foreach(DataRow drw in DS.Tables[0].Rows)
                foreach (DataGridViewRow dgrow in dgv.Rows)
                {
                    if (dgrow.Cells[0].Value != null && dgrow.Cells[1].Value != null)
                    if (drw.ItemArray[0].ToString().ToUpper() == dgrow.Cells[0].Value.ToString().ToUpper() && drw.ItemArray[1].ToString().ToUpper() == dgrow.Cells[1].Value.ToString().ToUpper())
                        return false;
                }
                

            return DoesNotExists;

        }


    }

  

}
